from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch, Mock, ANY
from generate_ticket import generate_ticket
from pony.orm import db_session, rollback

import settings
from vk_api.bot_longpoll import VkBotMessageEvent, VkBotEvent

from Bot import Bot

def isolate_db(test_func):
    def wrapper(*args, **kwargs):
        with db_session():
            test_func(*args, **kwargs)
            rollback()
    return wrapper
class Test(TestCase):
    RAW_EVENT = {
        'type': 'message_new',
        'object': {
            'date': 1612186768, 'from_id': 23090304, 'id': 16, 'out': 0, 'peer_id': 23090304,
            'text': 'Hello', 'conversation_message_id': 14, 'fwd_messages': [], 'important': False,
            'random_id': 0, 'attachments': [], 'is_hidden': False},
        'group_id': 201819950, 'event_id': '51f574727bb96cfd61ce29912da909924323c92a'}

    def test_run(self):
        objects = {}
        count = 5
        events = count * [objects]
        long_poller_mock = Mock(return_value=events)
        long_poller_listen_mock = Mock()
        long_poller_listen_mock.listen = long_poller_mock
        with patch("Bot.vk_api.VkApi"):
            with patch("Bot.VkBotLongPoll", return_value=long_poller_listen_mock):
                bot = Bot("", "")
                bot.on_event = Mock()
                bot.send_image = Mock()
                bot.run()
                bot.on_event.assert_called()
                bot.on_event.assert_any_call(objects)
                assert bot.on_event.call_count == count


    INPUTS = [
        "Привет",
        "А когда",
        "Где",
        "Зарегистрируй меня",
        "Аристарх",
        "Адрес мое эл почты sasdad",
        "gmail@gmail.ru"
    ]

    EXPECTED_OUTPUTS = [
        settings.DEFAULT_ANSWER,
        settings.INTENTS[0]["answer"],
        settings.INTENTS[1]["answer"],
        settings.SCENARIOS["registration"]["steps"]["step1"]["text"],
        settings.SCENARIOS["registration"]["steps"]["step2"]["text"],
        settings.SCENARIOS["registration"]["steps"]["step2"]["failure_text"],
        settings.SCENARIOS["registration"]["steps"]["step3"]["text"].format(name="Аристарх", email="gmail@gmail.ru"),
    ]
    @isolate_db
    def test_run_ok(self):
        send_mock = Mock()
        api_mock = Mock()
        api_mock.messages.send = send_mock
        events = []
        for input_text in self.INPUTS:
            event = deepcopy(self.RAW_EVENT)
            event["object"]["text"] = input_text
            events.append(VkBotEvent(event))
        long_poller_mock = Mock()
        long_poller_mock.listen = Mock(return_value=events)
        with patch("Bot.VkBotLongPoll", return_value=long_poller_mock):
            bot = Bot("", "")
            bot.api = api_mock
            bot.send_image = Mock()
            bot.run()
        assert send_mock.call_count == len(self.INPUTS)
        real_outputs = []
        for call in send_mock.call_args_list:
            args, kwargs = call
            real_outputs.append(kwargs["message"])
        assert real_outputs == self.EXPECTED_OUTPUTS


    def test_image_generation(self):
        with open ("files/sample_avatar.jpeg", "rb") as avatar_file:
            avatar_mock = Mock()
            avatar_mock.content = avatar_file.read()
        with patch("requests.get", return_value=avatar_mock):
            tiket_file = generate_ticket("name", "edsdsail")
        with open("files/ticket_example.png", "rb") as expected_file:
            assert tiket_file.read() == expected_file.read()
