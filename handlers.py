import re

from generate_ticket import generate_ticket

re_name = re.compile(r"^[\w\-\s]{3,40}$")
re_email = re.compile(r'\b[a-zA-Z0-9_.]+@[-a-z0-9]+\.[a-z]{2,6}\b')


def handle_name(text, context):
    """
    Возвращат нам истину если пользователь ввел корректное имя ользователя.

    """
    match = re.match(re_name, text)
    if match:
        context["name"] = text
        return True
    else:
        return False


def handle_email(text, context):
    """
        Возвращат нам истину если пользователь ввел корректный адрес эл.почты.

    """
    matches = re.findall(re_email, text)
    if len(matches) > 0:
        context["email"] = matches[0]
        return True
    else:
        return False

def generate_ticket_handler(text, context):
    """
        Вызывает метод generate_ticket с получеными из context именем и адресом эл.почты

    """
    return generate_ticket(name=context["name"], email=context["email"])
