from pony.orm import Database, Required, Json
from settings import DB_CONFIG_1

db = Database()
db.bind(**DB_CONFIG_1)


class UserState(db.Entity):

    user_id = Required(str, unique=True)
    scenario_name = Required(str)
    step_name = Required(str)
    context = Required(Json)


class Registration(db.Entity):

    user_name = Required(str)
    email = Required(str)

db.generate_mapping(create_tables=True)