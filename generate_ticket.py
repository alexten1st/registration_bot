from io import BytesIO

import requests
from PIL import Image, ImageDraw, ImageFont

TEMPLATE_PATH = "files/ticket_base.png"
FONT_PATH = "files/Roboto-Regular.ttf"
FONT_SIZE = 20
BLACK = (0, 0, 0, 255)
NAME_OFFSET = (180, 157)
EMAIL_OFFSET = (180, 185)
AVATAR_OFFSET = (30, 145)
AVATAR_SIZE = 80
def generate_ticket(name, email):
    """

    :param name: str - имя пользвателя
    :param email: str - адрес электронной почты пользователя
    :return: temp_file - возвращает сгенерированный билет с рандомным аватаром
    """
    base = Image.open(TEMPLATE_PATH).convert("RGBA")
    font = ImageFont.truetype(FONT_PATH, FONT_SIZE)
    draw = ImageDraw.Draw(base)
    draw.text(NAME_OFFSET, name, font=font, fill=BLACK)
    draw.text(EMAIL_OFFSET, email, font=font, fill=BLACK)
    response = requests.get(url=f"https://i.pravatar.cc/{AVATAR_SIZE}?u={email}")
    avatar_file_like = BytesIO(response.content)
    avatar = Image.open(avatar_file_like)
    base.paste(avatar, AVATAR_OFFSET)
    temp_file = BytesIO()
    base.save(temp_file, "png")
    temp_file.seek(0)
    return temp_file


print(generate_ticket('name', "edsdsail"))